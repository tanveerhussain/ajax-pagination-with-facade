<?php namespace Tanveerhussain\Pagination;

class Pagination  {

    public static function get_all_records($table=NULL){
		
		if(empty($table)){
			return array();
		}
		
		$result = \DB::table($table)->get();
		return $result;
		
    }
	
	public static function get_records_with_limit($table=NULL, $limit = 2){
		
		if(empty($table)){
			return array();
		}
		
		$result = \DB::table($table)->paginate($limit);
		return $result;
		
    }

}