<?php 
function humanTiming ($time)
	{
		
			$time = time() - $time; // to get the time since that moment
			$tokens = array (
				31536000 => 'year',
				2592000 => 'month',
				604800 => 'week',
				86400 => 'day',
				3600 => 'hour',
				60 => 'minute',
				1 => 'second'
			);
		
			foreach ($tokens as $unit => $text) {
				if ($time < $unit) continue;
				$numberOfUnits = floor($time / $unit);
				return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
			}
		
     }
?>

   <table align="left" border="0" width="100%" cellpadding="0" cellspacing="0" id="questions_listing">
      	<thead>
        	<tr>
            	<th align="left" valign="top" width="50%">Question Title</th>
                <th align="left" valign="top" width="12%">Question Type</th>
                <th align="left" valign="top" width="10%">Views</th>
                <th align="left" valign="top" width="15%">Created Date</th>
            </tr>
        </thead>
        <tbody>
          @if(!empty($questions))
          @foreach($questions as $value)
        	<tr>
            	<td align="left" valign="middle">{{ $value->question }}</td>
                <td align="left" valign="middle">{{ ucfirst($value->question_type) }}</td>
                <td align="left" valign="middle">{{ $value->num_views }}</td>
                <td align="left" valign="middle">{{ humanTiming(strtotime($value->created_at)) }} ago</td>
            </tr>
           @endforeach 
           @else
            <tr><td align="center" valign="top" colspan="4"><p class="error">No Record Found</p></td></tr>
           @endif 
        </tbody>
      </table>
      
      @if(!empty($questions))
      	<div class="pagination_place">{{ $questions->links() }}</div>
      @endif 
      
  <script>
  	
	$('ul.pagination a').on('click', function (event) {
		event.preventDefault();
		if ( $(this).attr('href') != '#' ) {
			$('#questions_listing tbody').html('<tr><td align="center" valign="top" colspan="4"><img src="/assets/images/ajaxloader.gif" width="128" height="128" ></td></tr>');
			$('.animate').load($(this).attr('href'));
		}
	});
  
  </script>
