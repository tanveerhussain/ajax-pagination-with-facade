<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6 lt8"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7 lt8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8 lt8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">  -->
<title>@yield('title')</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="" />
<meta name="keywords" content="" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/demo.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/style3.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/animate-custom.css')}}" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js" type="text/javascript"></script>
</head>
<body>
<div class="container">
  <header>
    <h1><a href="/" >Ajax Pagination <span>with Facades Class</span></a></h1>
  </header>
  <section>
    <div id="container_demo" > 
      @yield('content')
    </div>
  </section>
</div>

</body>
</html>