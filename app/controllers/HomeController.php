<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index()
	{
		$results = Pagination::get_records_with_limit('questions');
		if (Request::ajax()){
			sleep(1);
			return View::make('ajax_list_records')->with('questions', $results);
		}else{
			return View::make('list_records')->with('questions', $results);
		}
	}

}
